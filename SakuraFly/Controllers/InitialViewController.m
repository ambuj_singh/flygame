//
//  MainViewController.m
//  SakuraFly
//
//  Created by Chenglin on 15-10-1.
//  Copyright (c) 2015年 Chenglin. All rights reserved.
//

#import "InitialViewController.h"
#import "PrimaryScene.h"
#import "GameKitHeaders.h"

@import GameKit;

@interface InitialViewController ()

@property (strong, nonatomic) ADInterstitialAd *interstitial;
@property (strong, nonatomic) ADBannerView *adBanner;
@property (strong, nonatomic) PrimaryScene *mainScene;
@property (assign, nonatomic) BOOL gameCenterEnabled;
@property (assign, nonatomic) BOOL requestingAd;

@end


@implementation InitialViewController

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)loadView
{
    CGRect applicationFrame = [[UIScreen mainScreen] applicationFrame];
    SKView *skView = [[SKView alloc] initWithFrame: applicationFrame];
    self.view = skView;
#ifdef DEBUG
    skView.showsDrawCount = NO;
    skView.showsFPS = NO;
    skView.showsNodeCount = NO;
#else
    [self authenticateLocalPlayer];
#endif
    _mainScene = [[PrimaryScene alloc] initWithSize:CGSizeMake(skView.bounds.size.width, skView.bounds.size.height)];
    _mainScene.scaleMode = SKSceneScaleModeAspectFit;
    [_mainScene runAction:[SKAction repeatActionForever:[SKAction playSoundFileNamed:@"Halloween-doorbell.mp3" waitForCompletion:YES]]];
    [skView presentScene:_mainScene];
}

#pragma mark - game center

-(void)authenticateLocalPlayer{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
        if (viewController != nil) {
            [self presentViewController:viewController animated:YES completion:nil];
        }else{
            if ([GKLocalPlayer localPlayer].authenticated) {
                _gameCenterEnabled = YES;        }
            
            else{
                _gameCenterEnabled = NO;
            }
        }
    };
}




@end
